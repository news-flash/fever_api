use crate::deserialize::vec_ids_from_string;
use crate::FeedGroupId;
use crate::FeedId;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct FeedsGroups {
    pub group_id: FeedGroupId,
    #[serde(deserialize_with = "vec_ids_from_string")]
    pub feed_ids: Vec<FeedId>,
}
