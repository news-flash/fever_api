use crate::deserialize::*;
use crate::ItemId;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct SavedItems {
    #[serde(deserialize_with = "to_i64")]
    pub last_refreshed_on_time: i64,
    #[serde(deserialize_with = "vec_ids_from_string")]
    pub saved_item_ids: Vec<ItemId>,
}
