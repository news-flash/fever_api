use crate::deserialize::*;
use crate::models::feedgroup::FeedsGroups;
use crate::FeedId;
use crate::IconId;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Feed {
    #[serde(deserialize_with = "to_u64")]
    pub id: FeedId,
    #[serde(deserialize_with = "to_u64")]
    pub favicon_id: IconId,
    pub title: String,
    pub url: String,
    pub site_url: Option<String>,
    #[serde(deserialize_with = "bool_from_number")]
    pub is_spark: bool,
    #[serde(deserialize_with = "to_i64")]
    pub last_updated_on_time: i64,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Feeds {
    #[serde(deserialize_with = "to_i64")]
    pub last_refreshed_on_time: i64,
    pub feeds: Vec<Feed>,
    pub feeds_groups: Vec<FeedsGroups>,
}
