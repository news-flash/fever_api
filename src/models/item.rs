use crate::deserialize::*;
use crate::FeedId;
use crate::ItemId;

use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Item {
    #[serde(deserialize_with = "to_u64")]
    pub id: ItemId,
    #[serde(deserialize_with = "to_u64")]
    pub feed_id: FeedId,
    pub title: String,
    pub author: String,
    pub html: String,
    pub url: String,
    #[serde(deserialize_with = "bool_from_number")]
    pub is_saved: bool,
    #[serde(deserialize_with = "bool_from_number")]
    pub is_read: bool,
    #[serde(deserialize_with = "to_i64")]
    pub created_on_time: i64,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Items {
    #[serde(deserialize_with = "to_i64")]
    pub last_refreshed_on_time: i64,
    #[serde(deserialize_with = "to_u64")]
    pub total_items: u64,
    pub items: Vec<Item>,
}
