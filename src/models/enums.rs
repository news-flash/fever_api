use crate::error::ApiError;
use std::convert::{From, TryFrom};

#[derive(Copy, Clone)]
pub enum ItemStatus {
    Unread,
    Read,
    Saved,
    Unsaved,
}

impl From<ItemStatus> for &str {
    fn from(item: ItemStatus) -> Self {
        match item {
            ItemStatus::Unread => "unread",
            ItemStatus::Read => "read",
            ItemStatus::Saved => "saved",
            ItemStatus::Unsaved => "unsaved",
        }
    }
}

impl TryFrom<&str> for ItemStatus {
    type Error = ApiError;

    fn try_from(item: &str) -> Result<Self, ApiError> {
        let parsed = match item {
            "unread" => ItemStatus::Unread,
            "read" => ItemStatus::Read,
            "saved" => ItemStatus::Saved,
            "unsaved" => ItemStatus::Unsaved,
            _ => return Err(ApiError::Parse),
        };
        Ok(parsed)
    }
}
