use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct FeverError {
    #[serde(rename = "errorCode")]
    pub error_code: u32,
    #[serde(rename = "errorId")]
    pub error_id: String,
    #[serde(rename = "errorMessage")]
    pub error_message: String,
}

impl PartialEq for FeverError {
    fn eq(&self, other: &FeverError) -> bool {
        self.error_code == other.error_code
    }
}

impl Eq for FeverError {}
