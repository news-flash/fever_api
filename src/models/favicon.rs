use crate::deserialize::*;
use crate::IconId;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FavIconRequest {
    #[serde(deserialize_with = "to_u64")]
    pub id: IconId,
    pub data: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FavIcon {
    pub mime_type: String,
    pub data: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct FavIcons {
    #[serde(deserialize_with = "to_i64")]
    pub last_refreshed_on_time: i64,
    #[serde(deserialize_with = "to_favicon_hashmap")]
    pub favicons: HashMap<IconId, FavIcon>,
}
