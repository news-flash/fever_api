use crate::deserialize::*;
use crate::models::feedgroup::FeedsGroups;
use crate::GroupId;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Group {
    #[serde(deserialize_with = "to_u64")]
    pub id: GroupId,
    pub title: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Groups {
    #[serde(deserialize_with = "to_i64")]
    pub last_refreshed_on_time: i64,
    pub groups: Vec<Group>,
    pub feeds_groups: Vec<FeedsGroups>,
}
