use crate::deserialize::*;
use crate::FeedId;
use crate::ItemId;
use crate::LinkId;
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Link {
    #[serde(deserialize_with = "to_u64")]
    pub id: LinkId,
    #[serde(deserialize_with = "to_u64")]
    pub feed_id: FeedId,
    #[serde(deserialize_with = "to_u64")]
    pub item_id: ItemId,
    pub temperature: f64,
    #[serde(deserialize_with = "bool_from_number")]
    pub is_item: bool,
    #[serde(deserialize_with = "bool_from_number")]
    pub is_local: bool,
    #[serde(deserialize_with = "bool_from_number")]
    pub is_saved: bool,
    pub title: String,
    pub url: String,
    #[serde(deserialize_with = "vec_ids_from_string")]
    pub item_ids: Vec<ItemId>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Links {
    #[serde(deserialize_with = "to_i64")]
    pub last_refreshed_on_time: i64,
    pub links: Vec<Link>,
}
