use crate::models::ItemStatus;
use crate::FeverApi;
use reqwest::Client;
use std::env;
use url::Url;

fn test_setup_env(aggregator: &str) -> FeverApi {
    dotenv::dotenv().expect("Failed to read .env file");

    let url = env::var(format!("{}_URL", aggregator.to_uppercase()))
        .unwrap_or_else(|_| panic!("Failed to read {}_URL", aggregator.to_uppercase()));
    let user = env::var(format!("{}_USER", aggregator.to_uppercase()))
        .unwrap_or_else(|_| panic!("Failed to read {}_USER", aggregator.to_uppercase()));
    let pw = env::var(format!("{}_PW", aggregator.to_uppercase()))
        .unwrap_or_else(|_| panic!("Failed to read {}_PW", aggregator.to_uppercase()));

    let url = Url::parse(&url).unwrap();
    FeverApi::new(&url, &user, &pw)
}

#[tokio::test]
async fn api_version() {
    let api = test_setup_env("freshrss");

    let response = api.get_api_version(&Client::new()).await;
    assert!(response.is_ok());
    assert!(response.unwrap() == 3);
}

#[tokio::test]
async fn valid_credentials() {
    let api = test_setup_env("freshrss");

    let response = api.valid_credentials(&Client::new()).await;
    assert!(response.is_ok());
    assert!(response.unwrap());
}

#[tokio::test]
async fn groups() {
    let api = test_setup_env("freshrss");

    let response = api.get_groups(&Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn feeds() {
    let api = test_setup_env("freshrss");

    let response = api.get_feeds(&Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn favicons() {
    let api = test_setup_env("freshrss");

    let response = api.get_favicons(&Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn items() {
    let api = test_setup_env("freshrss");

    let response = api.get_items(&Client::new()).await;
    assert!(response.is_ok());

    let response = api.get_items_since(5, &Client::new()).await;
    assert!(response.is_ok());

    let response = api.get_items_max(22, &Client::new()).await;
    assert!(response.is_ok());

    let response = api
        .get_items_with(vec![5, 7, 8, 9, 10], &Client::new())
        .await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn links() {
    let api = test_setup_env("freshrss");

    let response = api.get_links(&Client::new()).await;
    assert!(response.is_ok());

    let response = api.get_links_with(0, 7, 1, &Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn unread() {
    let api = test_setup_env("freshrss");

    let response = api.get_unread_items(&Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn saved() {
    let api = test_setup_env("freshrss");

    let response = api.get_saved_items(&Client::new()).await;
    assert!(response.is_ok());
}

#[tokio::test]
async fn mark_item() {
    let api = test_setup_env("freshrss");

    let response = api.get_items_since(1, &Client::new()).await;
    assert!(response.is_ok());
    let items = &response.unwrap();
    assert!(!items.items.is_empty());
    let valid_item = &items.items[0];
    let id = valid_item.id;

    let response = api.get_saved_items(&Client::new()).await;
    assert!(response.is_ok());
    let saved_item_ids = response.unwrap().saved_item_ids;
    assert!(!saved_item_ids.contains(&id));
    let tmp: usize = saved_item_ids.len();

    let response = api.mark_item(ItemStatus::Saved, id, &Client::new()).await;
    assert!(response.is_ok());

    let response = api.get_saved_items(&Client::new()).await;
    assert!(response.is_ok());
    let saved_item_ids = response.unwrap().saved_item_ids;
    assert!(saved_item_ids.contains(&id));
    assert_eq!(tmp + 1, saved_item_ids.len());

    let response = api.mark_item(ItemStatus::Unsaved, id, &Client::new()).await;
    assert!(response.is_ok());

    let response = api.get_saved_items(&Client::new()).await;
    assert!(response.is_ok());

    let saved_item_ids = response.unwrap().saved_item_ids;
    assert!(!saved_item_ids.contains(&id));
    assert_eq!(tmp, saved_item_ids.len());
}
