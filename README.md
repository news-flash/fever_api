# fever_api &emsp; [![build]][pipeline] [![Latest Version]][crates.io] [![Rustc Version 1.31+]][rustc]

[Latest Version]: https://img.shields.io/crates/v/fever_api.svg
[crates.io]: https://crates.io/crates/fever_api
[build]: https://img.shields.io/gitlab/pipeline/news-flash/fever_api
[pipeline]: https://gitlab.com/news-flash/fever_api/-/pipelines
[Rustc Version 1.31+]: https://img.shields.io/badge/rustc-1.31+-lightgray.svg
[rustc]: https://blog.rust-lang.org/2018/12/06/Rust-1.31-and-rust-2018.html

tested with:
- [FreshRSS](https://www.freshrss.org/)
- [TTRSS](https://tt-rss.org/) (with [plugin](https://github.com/DigitalDJ/tinytinyrss-fever-plugin))
- [Fever](https://feedafever.com/)
